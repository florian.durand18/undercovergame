const room = {
    users: [],
    usersLimit: 8,
    isGameStarted: false,
    currentUser: {},
    countMrWhite: 0,
    countUndercover: 0,
    currentTurn: 0,
    timer: {
        isStarted: false,
        time: 5
    },
    roomOwner: 0,
    wordUndercover: '',
    wordCivil: '',
    nextUserToPlay: {},
}

const getters = {
    getUsersInRoom: (state) => {
        return state.room.users
    },
    getCurrentUser: (state) => {
        return state.room.currentUser
    },
    getRoomState: (state) => {
        return state.room
    },
    getNextUserToPlay: (state) => {
        return state.room.nextUserToPlay
    }
}

const actions = {
    setUsers({ commit }, users) {
        commit('SET_USERS', users)
    },
    removeUser({ commit }, user) {
        commit('REMOVE_USER', user)
    },
    setCurrentUser({ commit }, user) {
        commit('SET_CURRENT_USER', user)
    },
    addLocalStorageIdToUser({ commit }, id) {
        commit('ADD_LOCALSTORAGEID_TO_USER', id)
    },
    setIsGameStarted({ commit }, status) {
        commit('SET_ISGAMESTARTED', status)
    },
    setTimerTime({ commit }, time) {
        commit('SET_TIMERTIME', time)
    },
    setTimerIsStarted({ commit }, status) {
        commit('SET_TIMERISSTARTED', status)
    },
    setRoomOwner({ commit }, roomOwner) {
        commit('SET_ROOM_OWNER', roomOwner)
    },
    setNbUndercover({ commit }, nbUndercover) {
        commit('SET_ROOM_NB_UNDERCOVER', nbUndercover)
    },
    setNbMrWhite({ commit }, nbMrWhite) {
        commit('SET_ROOM_NB_MRWHITE', nbMrWhite)
    },
    setWordCivil({ commit }, wordCivil) {
        commit('SET_WORD_CIVIL', wordCivil)
    },
    setWordUndercover({ commit }, wordUndercover) {
        commit('SET_WORD_UNDERCOVER', wordUndercover)
    },
    setNextUserToPlay({ commit }, user) {
        commit('SET_NEXT_USER_TO_PLAY', user)
    }
}

const mutations = {
    SET_USERS(state, users) {
        state.room.users = users
    },
    REMOVE_USER(state, user) {
        state.room.users = state.room.users.filter(u => u.id !== user.id)
    },
    SET_CURRENT_USER(state, user) {
        state.room.currentUser = user
    },
    ADD_LOCALSTORAGEID_TO_USER(state, id) {
        state.room.currentUser['localStorageId'] = id
    },
    SET_ISGAMESTARTED(state, status) {
        state.room.isGameStarted = status
    },
    SET_TIMERTIME(state, time) {
        state.room.timer.time = time
    },
    SET_TIMERISSTARTED(state, status) {
        state.room.timer.isStarted = status
    },
    SET_ROOM_OWNER(state, roomOwner) {
        state.room.roomOwner = parseInt(roomOwner, 10)
    },
    SET_ROOM_NB_UNDERCOVER(state, nbUndercover) {
        state.room.countUndercover = parseInt(nbUndercover, 10)
    },
    SET_ROOM_NB_MRWHITE(state, nbMrWhite) {
        state.room.countMrWhite = parseInt(nbMrWhite, 10)
    },
    SET_WORD_CIVIL(state, wordCivil) {
        state.room.wordCivil = wordCivil
    },
    SET_WORD_UNDERCOVER(state, wordUndercover) {
        state.room.wordUndercover = wordUndercover
    },
    SET_NEXT_USER_TO_PLAY(state, user) {
        state.room.nextUserToPlay = user
    }
}

export default {
    state: () => ({ room }),
    getters,
    actions,
    mutations
}