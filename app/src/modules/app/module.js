const app = {
    menu: true,
}

export default {
    state: () => ({ app }),
    mutations: {
        EDIT_MENU: (state, payload) => {
            state.app.menu = payload
        }
    },
    actions: {
        editMenu: (store, payload) => {
            store.commit('EDIT_MENU', payload)
        }
    },
    getters: {
        getApp: (state) => {
            return state.app
        }
    },
}
