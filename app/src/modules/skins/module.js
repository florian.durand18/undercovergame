import { getSkins } from './service'

const skins = {
    skins: [],
    currentSkin: {}
}

export default {
    state: () => ({ ...skins }),
    mutations: {
        SET_SKINS: (state, payload) => {
            state.skins.skins = payload
        },
        SET_CURRENT_SKIN: (state, payload) => {
            payload["path"] = require('@/assets/images/skins/' + payload.name + '.svg')
            state.currentSkin = payload
        }
    },
    actions: {
        async setSkins(store) {
            const skins = await getSkins();
            store.commit('SET_SKINS', skins)
            store.commit('SET_CURRENT_SKIN', skins[0])
        },
        setCurrentSkin(store, payload) {
            store.commit('SET_CURRENT_SKIN', payload)
        }
    },
    getters: {
        getSkins: (state) => {
            return state.skins.skins
        },
        getMaleSkins: (state) => {
            return state.skins.skins.filter(skin => skin.type === 'm')
        },
        getFemaleSkins: (state) => {
            return state.skins.skins.filter(skin => skin.type === 'f')
        },
        getCurrentSkin: (state) => {
            return state.currentSkin
        }
    },
}
