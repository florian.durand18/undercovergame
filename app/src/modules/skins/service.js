import fetch from '../../config/fetch'

const SKINS_API_URL = (process.env.API_URL || 'http://localhost:3000/') + 'skins'

export const getSkins = async () => {
    return await fetch(SKINS_API_URL, {
        method: 'GET',
        headers: {
            'Content-type': 'application/ld+json',
            Accept: 'application/ld+json'
        },
    }).then(res => {
        if (res != null) {
            return res
        }
    }).catch(err => {
        if (err.json) {
            return err.json.then(json => {
                console.log(json)
            })
        }
    })
}
