import { createStore, createLogger } from 'vuex'
import app from '@/modules/app/module'
import skins from '@/modules/skins/module'
import room from '@/modules/room/module'

export default createStore({
    modules: {
        app,
        skins,
        room
    },
    plugins: process.env.NODE_ENV !== 'production'
    ? [createLogger()]
    : []
})
