import { createWebHistory, createRouter } from "vue-router";
import Home from '../views/Home'
import Contribute from '../views/Contribute'
import Play from '../views/Play'
import Room from '../views/Room'
import Join from '../views/Join'

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/contribuer',
        name: 'contribute',
        component: Contribute
    },
    {
        path: '/jouer',
        name: 'play',
        component: Play
    },
    {
        path: '/rejoindre/:id',
        name: 'join',
        component: Join
    },
    {
        path: '/jouer/:id',
        name: 'room',
        component: Room
    }
]
 
const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router
