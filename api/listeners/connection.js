const { Op, where } = require("sequelize");
const models = require("../models/index");
const User = models.User;
const Room = models.Room;
const SynoWord = models.SynoWord;

createOrGetUser = (io, idRoom, clientId, room, dataUser) => {
    if (!dataUser.localStorageId) dataUser.localStorageId = null;
    User.findOne({ where: {
        [Op.or]: [
            {
                socketId: clientId
            },
            {
                id: dataUser.localStorageId
            }
        ]},
        include: Room
    })
    .then((userFound) => {
        if (userFound) {
            userFound.update({
                RoomId: room.id,
                socketId: clientId,
                hasPlayed: false,
                isUndercover: false,
                isMrWhite: false
            })
            .then(() => {
                if (!room.roomOwner) {
                    room.update({
                        roomOwner: userFound.id
                    })
                }
                io.to(clientId).emit('setup', userFound)
                io.to(idRoom).emit('setupRoom', room)
                User.findAll({ where: { RoomId: room.id } })
                .then((users) => {
                    io.to(idRoom).emit('users', users)
                })
            })
        } else {
            User.create({
                username: dataUser.username,
                socketId: clientId,
                RoomId: room.id,
                skin: dataUser.skin.name,
                hasPlayed: false,
                isUndercover: false,
                isMrWhite: false
            }).then((user) => {
                if (!room.roomOwner) {
                    room.update({
                        roomOwner: user.id
                    })
                }
                io.to(clientId).emit('setup', user)
                io.to(idRoom).emit('setupRoom', room)
                User.findAll({ where: { RoomId: room.id } })
                .then((users) => {
                    io.to(idRoom).emit('users', users)
                })
            })
        }
    })
}

nextPlayToPlay = (io, room) => {
    User.findAll({ where: { 
        [Op.and]: [
            { RoomId: room.id },
            { hasPlayed: false }
        ]
    }})
    .then(users => {
        if (users) {
            const user = users[Math.floor(Math.random() * users.length)];
            user.update({
                hasPlayed: true,
            })
            .then(() => io.to(room.roomPartyId).emit('nextUserToPlay', user))
        } else {
            io.to(room.roomPartyId).emit('vote', { vote: 'yes' })
        }
    })
}

module.exports = function (io) {
    io.on('connection', client => {
        const clientId = client.id
        client.on('setup', data => {

            const idRoom = data.idRoom
            const dataUser = data.user

            client.join(idRoom)

            Room.findOne({ where: { roomPartyId: idRoom } })
            .then(room => {
                if (!room) {
                    Room.create({
                        roomPartyId: idRoom,
                        isGameStarted: false,
                        countUndercover: 1,
                        countMrWhite: 0,
                    }).then((room) => {
                        createOrGetUser(io, idRoom, clientId, room, dataUser)
                    })
                } else {
                    createOrGetUser(io, idRoom, clientId, room, dataUser)
                }
            })
        });
        
        client.on('disconnect', () => {
            User.findOne({ where: { socketId: clientId }, include: Room })
            .then((user) => {
                if(user.Room.isGameStarted) {
                    // TODO MAKE USER DISCONNECTED AND NOT REMOVED
                } else {
                    // Delete the user in the database if the game is started
                    io.to(user.Room.roomPartyId).emit('remove-user', user)
                    user.Room.removeUser(user)
                }
            })
        });

        client.on('startGame', data => {
            Room.findOne({ where: { roomPartyId: data.idRoom } })
            .then(room => {
                User.findAll({ where: { RoomId: room.id } })
                .then(users => {
                    const user = users[Math.floor(Math.random() * users.length)];
                    user.update({
                        isUndercover: true
                    }).then(() => {
                        SynoWord.findAll()
                        .then(synowords => {
                            const synoword = synowords[Math.floor(Math.random() * synowords.length)]
                            synoword.addRoom(room)
                            console.log(synoword)
                            io.to(room.roomPartyId).emit('game-started', { room: room, users: users, synoword: synoword })
                            nextPlayToPlay(io, room)    
                        })
                    })
                })
            })
        })

        client.on('newRound', data => {

        })
    });
}

// client.on('test', data => {
//     io.in('559A5BEG').fetchSockets().then(data => console.log(data.length))
//     io.to('559A5BEG').emit('testFromApi', 'data')
// });

