'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Rooms', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      isGameStarted: {
        type: Sequelize.BOOLEAN
      },
      countMrWhite: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      countUndercover: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      timePerTurn: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Rooms');
  }
};