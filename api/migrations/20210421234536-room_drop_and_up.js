'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'Rooms', // table name
        'roomPartyId', // new field name
        {
          type: Sequelize.STRING,
          allowNull: false,
          defaultValue: 'null'
        },
      ),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn(
        'Rooms', // table name
        'roomId', // new field namec
      ),
    ]);
  }
};
