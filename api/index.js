require('dotenv').config()
const express = require('express');
const RouterManager = require("./routes");
const cors = require('cors');
const app = express();
const bodyparser = require('body-parser')
const db = require("./models");
const port = 3000;
const initListeners = require("./listeners");

app.use(cors())
app.use(bodyparser.json());

RouterManager(app);

db.sequelize.sync().then(() => {
  console.log("re-sync db.");
});

const server = require('http').Server(app)

const io = require('socket.io')(server, {
  cors: {
      origin: process.env.APP_URL,
      methods: ["GET", "POST"]
  }
});

initListeners(io);

server.listen(port, () => {
  console.log(`Listening at http://localhost:${port}`)
})

