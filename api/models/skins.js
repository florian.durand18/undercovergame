'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Skin extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Skin.belongsTo(models.User)
    }
  };
  Skin.init({
    name: DataTypes.STRING,
    type: DataTypes.STRING,
    isFree: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Skin',
  });
  return Skin;
};