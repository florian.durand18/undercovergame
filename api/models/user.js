'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.User.hasOne(models.Skin)
      User.belongsTo(models.Room)
    }
  };
  User.init({
    username: DataTypes.STRING,
    socketId: DataTypes.STRING,
    isMrWhite: DataTypes.BOOLEAN,
    isUndercover: DataTypes.BOOLEAN,
    skin: DataTypes.STRING,
    hasPlayed: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};
