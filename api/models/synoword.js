'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class SynoWord extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      SynoWord.hasMany(models.Room, { as: 'rooms' })
    }
  };
  SynoWord.init({
    civilWord: DataTypes.STRING,
    undercoverWord: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'SynoWord',
  });
  return SynoWord;
};