const SkinRouter = require("./skins");
const UserRouter = require("./users");

const routerManager = (app) => {
    app.use("/skins", SkinRouter);
    app.use("/users", UserRouter);
};

module.exports = routerManager;
