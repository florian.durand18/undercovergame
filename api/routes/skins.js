const express = require("express");
const models = require("../models/index");
const Skin = models.Skin;
const router = express.Router();

router.get("/", (req, res) => {
    Skin.findAll({
    })
      .then((data) => res.json(data))
        .catch((err) => res.sendStatus(500));
});

router.post("/", (req, res) => {
    Skin.create({
        name: req.body.name,
        type: req.body.type,
        isFree: req.body.isFree
    }).then(user => res.send(user));
});

module.exports = router